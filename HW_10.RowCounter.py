from pyspark.sql import SparkSession

if __name__ == "__main__":
    # Ініціалізуємо SparkSession
    spark = SparkSession.builder \
        .appName("Count Rows") \
        .getOrCreate()

    # Завантажуємо CSV файл
    df = spark.read.csv("file:///data.csv", header=True)

    # Підраховуємо кількість рядків
    row_count = df.count()

    # Виводимо кількість рядків на екран
    print("Number of rows:", row_count)

    # Зупиняємо SparkSession
    spark.stop()
